let items = require('./3-arrays-vitamins.cjs');

let itemsByVitamins=items.reduce((acc,item)=>{
  let vitamins=item.contains.split(", ");
  vitamins.forEach(vitamin=>{
    acc[vitamin]=acc[vitamin] || [];
    acc[vitamin].push(item.name); 
  } )
  return acc;
},{});

console.log(itemsByVitamins);